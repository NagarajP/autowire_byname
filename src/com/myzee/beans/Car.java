package com.myzee.beans;

import org.springframework.beans.factory.annotation.Autowired;

public class Car {
	private String carName;
	@Autowired
	private Engine engine1;
	
	public void setEngine(Engine engine1) {
		this.engine1 = engine1;
	}
	
	public void setCarName(String carName) {
		this.carName = carName;
	}
	
	public void printData() {
		System.out.println(this.carName + ":" + this.engine1.getModelYear());
	}
	
}
